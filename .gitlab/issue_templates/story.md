## Story
Descrizione della storia. Ovvero cosa l'utente (loggato e non) deve poter fare e visualizzare

## Dependencies
Specificare le eventuali dipendenze da altre storia

## Technical Tasks
Vanno inseriti i riferimenti a tutti i technical task creati per questa storia.
Vanno aggiunti durante il flusso di lavorazione della storia