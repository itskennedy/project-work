## Task
Descrizione dell'issue

## Concluded
Specificare quando l'issue si intende conclusa. 
Vanno specificati anche i test funzionali da effettuare

## Project issue
Qui va riportato il riferimento all'issue di progetto che va lavorata. Questo capitolo è necessario quando una board si occupa di progetti presenti in repository diversi

## Dependencies
Specificare le eventuali dipendenze dell'issue