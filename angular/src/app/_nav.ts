import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
    children: [
      {
        name: 'Link 1',
        url: '#',
        icon: 'icon-puzzle'
      },
      {
        name: 'Link 2',
        url: '#',
        icon: 'icon-puzzle'
      }
    ]
  }
];
